#ifndef FILE_READER_H
#define FILE_READER_H

#include "wind_rose.h"

void read(const char* file_name, WindRose* array[], int& size);

#endif
