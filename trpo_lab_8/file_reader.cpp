#include <fstream>
#include <string>

#include "file_reader.h"

void read(const char* file_name, WindRose* array[], int& size) {
  
  std::ifstream file(file_name); // создаем объект file класса ifstream и открываем его
  
  if (file.is_open()) {
    size = 0;
    while (!file.eof()) {
      WindRose * element = new WindRose;
      
      file >> element->day;
      file >> element->month;
      file >> element->Direction;
      file >> element->speed;
      
      array[size++] = element;
    }
    file.close(); // закрываем файл для освобождения ресурсов
  } else {
    throw "  ";
  }
}
