#include <iostream>
#include <iomanip>

using namespace std;

#include "wind_rose.h"
#include "constants.h"
#include "file_reader.h"
#include "filter.h"

void output(WindRose* wind_rose) {
  
  cout << "Дата: ";
  cout << setw(2) << setfill('0') << wind_rose->day << ".";
  cout << wind_rose->month;
  cout << endl;
  
  cout << "Направление: ";
  cout << wind_rose->Direction;
  cout << endl;
  
  cout << "Скорость: ";
  cout << wind_rose->speed << " м/с";
  cout << endl;
  cout << endl;
  
}

int main() {
  cout << "Лабораторная работа №8. GIT" << endl;
  cout << "Вариант №6. Роза ветров" << endl;
  cout << "Автор: Александр Каторжевский" << endl;
  cout << "Группа: 16" << endl << endl;
  
  WindRose* wind_rose[MAX_FILE_ROWS_COUNT]; // массив структур
  int size;
  
  try {
    read("data.txt", wind_rose, size);
    cout << "------ Роза ветров ------" << endl;
    for (int i = 0; i < size - 1; i++) {
      output(wind_rose[i]);
    }
    bool (*check_function)(WindRose*) = NULL; // it's magic
    
    cout << "\nСпособ фильтрации данных:\n";
    cout << "1) Все дни, в которые дул ветер в одном из направлений West, NorthWest или North." << endl;
    cout << "2) Все дни, в которые дул ветер больше 5 м/с." << endl;
    cout << "Введите номер фильтрации данных: ";
    int choice;
    cin >> choice;
    
    switch (choice) {
      
      case 1:
        check_function = check_wind_rose_by_direction; //
        cout << endl << "------ По направлению ------" << endl;
        break;
      
      case 2:
        check_function = check_wind_rose_by_speed; //
        cout << "------ По скорости ------" << endl;
        break;
      
      default:
        throw "  ";
    }
    
    if (check_function) {
      int new_size;
      WindRose** filtered = filter(wind_rose, size, check_function, new_size);
      
      for (int i = 0; i < new_size; i++) {
        output(filtered[i]);
      }
      
      delete[] filtered;
    }

    for (int i = 0; i < size; i++) delete wind_rose[i];
    
  }
  catch (const char* error) {
    cout << error << endl;
  }

  return 0;
}
