#ifndef WIND_ROSE_H
#define WIND_ROSE_H

#include <string>

enum Direction {
  North     = 1,
  NorthEast = 2,
  East      = 3,
  SouthEast = 4,
  South     = 5,
  SouthWest = 6,
  West      = 7,
  NorthWest = 8
};

struct WindRose {
  unsigned short day;    // день — целое неотрицательное число
  unsigned short month;  // месяц — целое неотрицательное число
  std::string Direction; // направление — строка, не содержащая пробелов — направление ветра, которое может быть одним из следующих значений: North, NorthEast, East, SouthEast, South, SouthWest, West, NorthWest
  float speed; // скорость — дробное неотрицательное число — скорость ветра в м/с
};

#endif
