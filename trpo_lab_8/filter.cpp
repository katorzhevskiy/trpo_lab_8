#include "filter.h"
#include <string>
#include <iostream>

WindRose** filter(WindRose* array[], int size, bool (*check)(WindRose* element), int& result_size)
{
  WindRose** result = new WindRose*[size];
  result_size = 0;
  for (int i = 0; i < size; i++)
  {
    if (check(array[i]))
    {
      result[result_size++] = array[i];
    }
  }
  return result;
}

bool check_wind_rose_by_direction(WindRose* element) {
  return (element->Direction == "North") || (element->Direction == "West") || (element->Direction == "NorthWest");
}

bool check_wind_rose_by_speed(WindRose* element) {
  return element->speed > 5;
}
